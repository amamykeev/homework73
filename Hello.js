const Vigenere = require('caesar-salad').Vigenere;
const express = require('express');
const app = express();

const port = 6900;

const password = 'parol';

app.get('/:name', (req, res) => {
   res.send('' + req.params.name);
});

app.get('/encode/:text', (req, res) => {
    const encodetext = Vigenere.Cipher(password).crypt(req.params.text);
    res.send(encodetext)
});

app.get('/decode/:text', (req, res) => {
    const decodetext = Vigenere.Decipher(password).crypt(req.params.text);
    res.send(decodetext)
});


app.listen(port, () => {
    console.log('We are live on ' + port);
});